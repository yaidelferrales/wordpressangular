#!/bin/bash

echo "Deploying new Theme into Wordpress root directory ..."
cp -R theme/* ../wp-content/themes/
cp -R plugins/* ../wp-content/plugins/
cp -R uploads/* ../wp-content/uploads/

echo "Changing wordpress directory owner to 'www-data' ..."
chown www-data:www-data -R ../*

echo "Applying wordpress permissions to all wordpress drectory ..."
find ../ -type d -exec chmod 755 {} \;
find ../ -type f -exec chmod 644 {} \;

echo "Migrating database ..."
WPDBNAME=`cat ../wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat ../wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat ../wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`

query=""
while read line
do 
    query+="DROP TABLE IF EXISTS $line; "
done < <(mysql -B -u ${WPDBUSER} -p${WPDBPASS} --disable-column-names -e "SELECT table_name FROM information_schema.tables WHERE table_schema='${WPDBNAME}' and table_name != 'wp_users'")

mysql -u $WPDBUSER -p$WPDBPASS -D$WPDBNAME -e "SET FOREIGN_KEY_CHECKS = 0; $query"
mysql -u $WPDBUSER -p$WPDBPASS -D$WPDBNAME < database.sql