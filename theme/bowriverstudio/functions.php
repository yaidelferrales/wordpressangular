<?php

function bowriverstudio_setup () {
    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
//    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu',      'anjularjsbowriverstudio' ),
        'more' => __( 'More About Us',      'anjularjsbowriverstudio' ),
    ) );
}

add_action( 'after_setup_theme', 'bowriverstudio_setup' );

function my_theme_add_editor_styles() {
    $styles = array(
        'css/bootstrap/bootstrap.css',
        'css/local/layout.css',
        'css/local/components.css',
    );
    add_editor_style( $styles );
}
add_action( 'admin_init', 'my_theme_add_editor_styles' );

function home_page_menu_args( $args ) {
    $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );

function theme_scripts() {
    //Load local styles
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap/bootstrap.css', array(), '3.3.6' );
    wp_enqueue_style( 'layout', get_template_directory_uri() . '/css/local/layout.css', array('bootstrap'), '1.0' );
    wp_enqueue_style( 'components', get_template_directory_uri() . '/css/local/components.css', array('bootstrap'), '1.0' );

	wp_register_script(
		'tjquery', 'https://code.jquery.com/jquery-1.12.0.min.js'
	);

    wp_register_script(
        'bootstrap',
        get_stylesheet_directory_uri() . '/js/libs/bootstrap.min.js',
        array( 'tjquery' ),
        '',
        true
    );

    // Angular scripts
    wp_register_script(
		'angularjs',
		get_stylesheet_directory_uri() . '/js/libs/angular/angular.min.js',
        array(),
        '',
        true
	);

	wp_register_script(
		'angularjs-route',
		get_stylesheet_directory_uri() . '/js/libs/angular/angular-route.min.js',
        array(),
        '',
        true
	);

	wp_register_script(
		'angularjs-sanitize',
		get_stylesheet_directory_uri() . '/js/libs/angular/angular-sanitize.min.js',
        array(),
        '',
        true
	);

	wp_register_script(
		'angular-animate',
		get_stylesheet_directory_uri() . '/js/libs/angular/angular-animate.min.js',
        array(),
        '',
        true
	);

	wp_register_script(
		'angular-scroll',
		get_stylesheet_directory_uri() . '/js/libs/angular/angular-scroll.min.js',
        array(),
        '',
        true
	);

//	wp_register_script(
//        'ui-bootstrap',
//        get_stylesheet_directory_uri() . '/js/libs/ui-bootstrap-tpls-1.1.2.min.js',
//        array('bootstrap'),
//        '',
//        true
//	);

    wp_register_script(
        'angular-app',
        get_stylesheet_directory_uri() . '/js/app.js',
        array( 'angularjs', 'angularjs-route', 'angularjs-sanitize', 'angular-animate', 'bootstrap', 'angular-scroll'),/*, 'ui-bootstrap'*/
        '',
        true
    );

	wp_register_script(
        'directives',
        get_stylesheet_directory_uri() . '/js/directives.js',
        array('angular-app'),
        '',
        true
	);

    wp_enqueue_script(
        'controllers',
        get_stylesheet_directory_uri() . '/js/controllers.js',
        array('directives'),
        '',
        true
	);

	wp_enqueue_script(
		'local',
		get_stylesheet_directory_uri() . '/js/theme-scripts.js',
        array('bootstrap'),
        '',
        true
	);

	wp_localize_script(
		'angular-app',
		'myLocalized',
		array(
			'partials' => trailingslashit( get_template_directory_uri() ) . 'partials/',
			'theme_imgs' => trailingslashit( get_template_directory_uri() ) . 'imgs/',
			)
	);
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

function myextensionTinyMCE($init) {
    // Command separated string of extended elements
    $ext = 'span[id|name|class|style]';

    // Add to extended_valid_elements if it alreay exists
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'myextensionTinyMCE' );

// add_filter('show_admin_bar', '__return_false');