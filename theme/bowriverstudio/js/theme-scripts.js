/**
 * Created by Alejandro on 11/27/2015.
 */


//determine if it is a touch device
function is_touch_device() {
    return (('ontouchstart' in window)
        || (navigator.MaxTouchPoints > 0)
        || (navigator.msMaxTouchPoints > 0));
};






//================================ TOOLTIP TRIGGERS =================================
$(function () {
    if(!is_touch_device()){
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
        $('.tooltip-trigger').tooltip({
            container: 'body'
        });
    }

    $('[data-toggle="popover"]').popover({
        container: 'body'
    });
});
//===================================================================================




//================================== DOT DOT DOT ====================================
$(document).ready(function() {
    try{
        $(".paragraph-ellipsis").dotdotdot({
            'ellipsis': '... ', // The HTML to add as ellipsis.
            'wrap': 'letter', // How to cut off the text/html: 'word'/'letter'/'children'
            'fallbackToLetter': true, // Wrap-option fallback to 'letter' for long words
            'lastCharacter': {},
            'tolerance': 0, // Deviation for the height-option.
            'callback': null, // Callback function
            'after': null, // jQuery-selector for the element to keep and put after the ellipsis.
//                    'height': '50px', // Optionally set a max-height, if null, the height will be measured.
            'watch': true, // Whether to up<a href="http://www.jqueryscript.net/time-clock/">date</a> the ellipsis: true/'window'.
            'windowResizeFix': true
        });
    }
    catch(err){

    }
});
//===================================================================================




//================================== DATA TABLES ====================================
$(document).ready(function() {
    try{
        $('#data-table').DataTable({
            responsive: true,
            "columnDefs": [
                { "orderable": false, "targets": [4] }
            ],
            "language": {
                "paginate": {
                    "next": "<i class='fa fa-chevron-right'></i>",
                    "previous": "<i class='fa fa-chevron-left'></i>"
                }
            }
        });
    }
    catch(err){

    }
} );
//======================================================================================




//========================== CLOSE NAVBAR ON CLICK OUTSIDE =============================
$(document).ready(function () {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("in");
        if (_opened == true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });
});
//======================================================================================




//============================== SHOW DIMMER LOADER ====================================
$(".dimmer-loader-trigger").on("click", function() {
    event.preventDefault();

    $(".dimmer-loader").toggleClass('hidden');
    $(".dimmer-loader .fa").toggleClass('fa-spin');
    setTimeout(function(){
        $(".dimmer-loader").toggleClass('hidden');
        $(".dimmer-loader .fa").toggleClass('fa-spin');
    }, 3000);
});

function showDimmer () {
//            event.preventDefault();

    $(".dimmer-loader").toggleClass('hidden');
    $(".dimmer-loader .fa").toggleClass('fa-spin');
    setTimeout(function(){
        $(".dimmer-loader").toggleClass('hidden');
        $(".dimmer-loader .fa").toggleClass('fa-spin');
    }, 3000);
};
//======================================================================================




//============================== QUANTITY GROUP ====================================
$(".input-group.quantity-selector .btn").on("click", function() {

    var $button = $(this);
    var oldValue = $button.parent().parent().find("input").val();
    if((oldValue == '')||(oldValue<1)||(!$.isNumeric(oldValue))){
        oldValue = 0;
    }

    if ($button.hasClass('increase-btn')) {
        var newVal = parseFloat(oldValue) + 1;
    } else {
        // Don't allow decrementing below 1
        if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 1;
        }
    }

    if(newVal === 0){
        $button.parent().parent().find("input").val('1');
    } else{
        $button.parent().parent().find("input").val(newVal);
    }

});
$("input.check-equal-greater-one").on("blur", function() {

    var $input = $(this);
    var oldValue = $input.val();

    //remove decimal places
    oldValue = Math.floor(oldValue);
    $input.val(oldValue);

    if( (!$.isNumeric(oldValue))  ||  ($.isNumeric(oldValue) && (oldValue<1))  ){
        $input.val(1);
    }

});
//======================================================================================




//========================= EQUAL HEIGHT COLS INSIDE A ROW =============================
var resizeRowCols = function () {
    $('.equal-height-row').each(function() {
        var colBiggestHeight = 0;
        $(this).children('div').css('min-height','');

        if (window.innerWidth > 768){
            $(this).children('div').each(function(){
                if ($(this).height() > colBiggestHeight) {
                    colBiggestHeight = $(this).height();
                }
            });
//            $(this).children().find('.panel').height(equalRowHeighBiggestHeight);
            $(this).children('div').css('min-height',colBiggestHeight);
        }
        else{
            $(this).children('div').css('min-height','');
        }
    })
};

$(window).on('resize load', function (e) {
    resizeRowCols();
});
//======================================================================================




//*********************date picker
//try{
//    $('.input-daterange').datepicker({
//        orientation: "top auto",
//        todayHighlight: true,
//        clearBtn: true,
//        container: 'body'
//    });
//}
//catch(err){
//
//}
//
//try{
//    $('input.datepicker').datepicker({
//        todayHighlight: true,
//        startDate: 'today'
//    });
//}
//catch(err){
//
//}


//$('body').on('click', function (e) {
//    $('[data-toggle="popover"]').each(function () {
//        //the 'is' for buttons that trigger popups
//        //the 'has' for icons within a button that triggers a popup
//        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
//            $(this).popover('hide');
//        }
//    });
//});



//********************* ANIMATIONS
//to animate when leaving the page, not sure if this is good, but looks good. It is up to the developer
//$('.go-out-animate-button').on('click',function(){
//    event.preventDefault();
//    var link = $(this).attr('href');
//
//    $('.go-out-to-right').addClass('bounceOutRight');
//    $('.go-out').addClass('fadeOut');
//    $('.go-out-to-left').addClass('bounceOutLeft');
//
//    $('.go-out-to-right').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
//        window.location.href = link;
//    });
//});