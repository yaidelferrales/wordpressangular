(function (angular) {
    'use strict';

    app.directive("ngPage", ['$http', '$sce', function ($http, $sce) {
        return {
            restrict: 'AC',
            scope: {
                page: '@ngPage'
            },
            templateUrl: myLocalized.partials + 'content.html',
            link: function (scope, element) {
                $http.get(urlBase + 'pages?filter[name]=' + scope.page)
                    .success(function (res) {
                        if (res.length == 0) {
                            scope.error404 = true;
                        } else {
                            scope.post = res[0];
                            $sce.trustAsHtml(scope.post.content.rendered);
                        }
                        element.attr('ready', true);
                    }).error(function (error) {
                        scope.error404 = true;
                        element.attr('ready', true);
                    });
            }
        };
    }]);

    app.directive("ngTeamManager", ['$http', function ($http) {
        return {
            restrict: 'AC',
            scope: {},
            templateUrl: myLocalized.partials + 'templates/team_member.html',
            link: function (scope, element) {
                $http.get(urlBase + 'team_manager')
                    .success(function (res) {
                        if (res.length == 0) {
                            scope.error404 = true;
                        } else {
                            scope.teamMembers = res;
                        }
                    }).error(function (error) {
                        scope.error404 = true;
                    });

                scope.imageClass = function (index) {
                    if (index % 2 == 0) return "no-padding-right padding-top-bottom-half-gutter text-right";
                    return "no-padding-left padding-top-bottom-half-gutter pull-right";
                };
                scope.imageData = function (index) {
                    if (index % 2 == 0) return "";
                    return "text-right";
                };
            }
        };
    }]);

    app.directive("ngTags", ['$http', function ($http) {
        return {
            restrict: 'AC',
            scope: {},
            templateUrl: myLocalized.partials + 'templates/tags.html',
            link: function (scope, element) {
                $http.get(urlBase + 'tags')
                    .success(function (res) {
                        scope.items = res;
                    }).error(function (error) {
                        scope.error = true;
                    });
            }
        };
    }]);

    app.directive("ngPosts", ['$http', '$location', '$rootScope', '$timeout', function ($http, $location, $rootScope, $timeout) {
        return {
            restrict: 'AC',
            scope: {
                template: '@template',
                content: '@content',
                paginate: '@paginate',
                infiniteScroll: '@infinitescroll',
                dependOnUrl: '@dependonurl',
                perPage: '@perpage',
                category: '@category',
                tag: '@tag',
                featured: '@featured'
            },
            template: '<ng-include src="templateUrl"></ng-include>',
            link: function (scope, element) {
                scope.content = scope.content != undefined ? scope.content : 'posts';
                scope.template = scope.template != undefined ? scope.template : 'posts';
                scope.paginate = (scope.paginate != undefined && (scope.paginate == 'true' || scope.paginate == 'false')) ? eval(scope.paginate) : true;
                scope.infiniteScroll = (scope.infiniteScroll != undefined && (scope.infiniteScroll == 'true' || scope.infiniteScroll == 'false')) ? eval(scope.infiniteScroll) : false;
                scope.perPage = (scope.perPage != undefined && !isNaN(scope.perPage)) ? (parseInt(scope.perPage) + '') : '10';
                scope.dependOnUrl = (scope.dependOnUrl != undefined && (scope.dependOnUrl == 'true' || scope.dependOnUrl == 'false')) ? eval(scope.dependOnUrl) : true;
                scope.templateUrl = myLocalized.partials + 'templates/' + scope.template + '.html';
                scope.category = (scope.category != undefined && !isNaN(scope.category)) ? (parseInt(scope.category) + '') : null;
                scope.tag = (scope.tag != undefined && !isNaN(scope.tag)) ? (parseInt(scope.tag) + '') : null;
                scope.featured = (scope.featured != undefined && scope.featured == 'yes') ? 'yes' : 'no';

                scope.options = {
                    //Default Options
                    sortOptions: [
                        {name: "Date published", value: 'date'},
                        {name: "Title", value: 'title'}
                    ],
                    itemsPerPageOptions: ['10', '25', '50', '100'],

                    //Pagination vars
                    pages: [],
                    totalPages: 1,
                    paginateTemplate: myLocalized.partials + 'templates/paginate.html'
                };

                //Options
                scope.options.query = '';
                scope.options.sort = scope.options.sortOptions[0].value;
                scope.options.sortOrderDesc = true;
                scope.options.itemsPerPage = scope.perPage;
                scope.options.page = 1;

                scope.data = {
                    items: []
                };


                scope.isValidOrderBy = function (value) {
                    for (var i = 0; i < scope.options.sortOptions; i++) {
                        if (scope.options.sortOptions[i].value == value)
                            return true;
                    }
                    return false;
                };

                scope.isValidItemsPerPage = function (value) {
                    for (var i = 0; i < scope.options.itemsPerPageOptions; i++) {
                        if (scope.options.sortOptions[i] == value)
                            return true;
                    }
                    return false;
                };

                scope.updateOptionsFromUrl = function () {
                    //Check search params
                    var searchParamaters = $location.search();

                    scope.options.query = (searchParamaters['s']) ? searchParamaters['s'] : "";
                    if (!scope.infiniteScroll) {
                        scope.options.page = (searchParamaters['page'] && !isNaN(searchParamaters['page'])) ? searchParamaters['page'] : 1;
                        scope.options.itemsPerPage = (searchParamaters['per_page'] && scope.isValidItemsPerPage(searchParamaters['per_page'])) ? searchParamaters['per_page'] : (scope.perPage);
                        scope.options.sortOrderDesc = (searchParamaters['order'] && (searchParamaters['order'] == 'asc' || searchParamaters['order'] == 'desc')) ? searchParamaters['order'] == 'desc' :  'desc';
                        scope.options.sort = (searchParamaters['orderby'] && scope.isValidOrderBy(searchParamaters['orderby'])) ? searchParamaters['orderby'] : 'date';
                        scope.category = (searchParamaters['category_name']) ? searchParamaters['category_name'] : null;
                        scope.tag = (searchParamaters['tag']) ? searchParamaters['tag'] : null;
                        scope.featured = (searchParamaters['featured'] && searchParamaters['featured'] == 'yes') ? 'yes' : 'no';
                    }
                };

                scope.update = function () {
                    var params = {
                        "page": scope.options.page,
                        "per_page": scope.options.itemsPerPage,
                        order: scope.options.sortOrderDesc ? 'desc' : 'asc',
                        orderby: scope.options.sort,
                        featured: scope.featured
                    };

                    if (scope.options.query.length > 0) {
                        params["search"] = scope.options.query;
                    }

                    if (scope.tag) {
                        params["tag"] = scope.tag;
                    }

                    if (scope.category) {
                        params["category_name"] = scope.category;
                    }

                    if (scope.dependOnUrl) {
                        scope.updateUrl();
                    }

                    if (scope.lastParams == params)
                        return;

                    scope.lastParams = angular.copy(params, {});

                    $http({
                        method: 'GET',
                        url: urlBase + scope.content + '?' + $.param(params)
                    }).then(function (res) {
                        var responseHeaders = res.headers();
                        if (responseHeaders['x-wp-totalpages'] && !isNaN(responseHeaders['x-wp-totalpages'])) {
                            scope.options.totalPages = parseInt(responseHeaders['x-wp-totalpages']);
                            scope.options.total = parseInt(responseHeaders['x-wp-total']);
                        } else {
                            scope.options.totalPages = 1;
                        }

                        if (scope.options.page > scope.options.totalPages) {
                            scope.options.page = 1;
                            if (scope.options.totalPages != 0)
                                return scope.update();
                        }

                        if (!scope.infiniteScroll || scope.options.page == 1) {
                            scope.data.items = res.data;
                        } else {
                            scope.data.items = scope.data.items.concat(res.data);
                        }

                        if(scope.paginate) {
                            scope.updatePagination();
                        }
                    }, function (error) {
                        scope.error404 = true;
                    });
                };

                scope.changeQuery = function () {
                    if (scope.promise) {
                        $timeout.cancel(scope.promise);
                    }

                    scope.promise = $timeout(function() {
                        scope.page = 1;
                        scope.update();
                        scope.promise = undefined;
                    }, 300);
                };

                scope.loadMore = function () {
                    if (scope.infiniteScroll && scope.options.page < scope.options.totalPages)
                        scope.options.page ++;
                    scope.update();
                };

                scope.clearQuery = function () {
                    scope.options.query = "";
                    scope.update();
                };

                scope.updateUrl = function () {
                    scope.updatingUrl = true;                    
                    $location.search({});

                    if (!scope.infiniteScroll) {
                        if (scope.options.page != 1)
                            $location.search('page', scope.options.page);

                        if (scope.options.itemsPerPage != '10')
                            $location.search('per_page', scope.options.itemsPerPage);

                        if (scope.options.sort != scope.options.sortOptions[0].value)
                            $location.search('orderby', scope.options.sort);

                        if (!scope.options.sortOrderDesc)
                            $location.search('order', 'asc');

                        if (scope.tag) {
                            $location.search('tag', scope.tag);
                        }

                        if (scope.category) {
                            $location.search('category_name', scope.category);
                        }
                    }

                    if (scope.options.query.length > 0)
                        $location.search('s', scope.options.query);

                    scope.updatingUrl = false;
                }

                scope.updatePagination = function () {
                    scope.options.pages = [];
                    var inicio = 1, fin = 1;
                    if (scope.options.totalPages <= 5) {
                        fin = scope.options.totalPages;
                    } else if (scope.options.page > 2 && scope.options.page + 2 <= scope.options.totalPages) {
                        inicio = scope.options.page - 2;
                        fin = inicio + 4;
                    } else if (scope.options.page > 2) {
                        fin = scope.options.totalPages;
                        inicio = fin - 4;
                    } else if (scope.options.page + 2 <= scope.options.totalPages) {
                        fin = inicio + 4;
                    }
                    for (var i = inicio; i <= fin; i++) {
                        scope.options.pages.push(i);
                    }
                };

                scope.isActivePage = function (page) {
                    return scope.options.page == page ? 'active' : '';
                };

                scope.goToPage = function (page) {
                    if (scope.options.page == page) {
                        return;
                    }
                    scope.options.page = page;
                    scope.update();
                };

                scope.getDate = function (date) {
                    return new Date(date);
                };
                
                if (scope.dependOnUrl) {
                    scope.$on('$routeUpdate', function(){
                        console.log($location.url())
                        if (!scope.updatingUrl) {
                            scope.updateOptionsFromUrl();
                            scope.update();
                        }
                    });
                }

                if (scope.dependOnUrl) {
                    scope.updateOptionsFromUrl();
                }
                scope.update();
            }

        };
    }]);

    app.directive('backImg', function () {
        return function (scope, element, attrs) {
            var url = attrs.backImg;
            element.css({
                'background-image': 'url(' + url + ')',
                "background-repeat": "no-repeat",
                "background-position": "center"
            });
        };
    });

    app.directive('bindHtmlCompile', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.bindHtmlCompile);
                }, function (value) {
                    // Incase value is a TrustedValueHolderType, sometimes it
                    // needs to be explicitly called into a string in order to
                    // get the HTML string.
                    element.html(value && value.toString());
                    // If scope is provided use it, otherwise use parent scope
                    var compileScope = scope;
                    if (attrs.bindHtmlScope) {
                        compileScope = scope.$eval(attrs.bindHtmlScope);
                    }
                    $compile(element.contents())(compileScope);
                });
            }
        };
    }]);

    app.directive('staticInclude', function ($http, $templateCache, $compile) {
        return function (scope, element, attrs) {
            var templatePath = attrs.staticInclude;
            $http.get(templatePath, { cache: $templateCache }).success(function (response) {
                var contents = element.html(response).contents();
                $compile(contents)(scope);
            });
        };
    });

})(window.angular);