(function (angular) {
    'use strict';

    app.controller('MenuCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {

        $scope.location = $location;
        $scope.init = function (menuId) {
            $http.get('wp-json/wp-api-menus/v2/menu-locations/' + menuId).success(function (res) {
                $scope.menuItems = res;
            });
        };
        $scope.getUrl = function (menuItem) {
            if (!menuItem.object)
                return "#";
            if (menuItem.children.length > 0)
                return "";

            var href;

            if (menuItem.object == 'custom'/* && menuItem.url.charAt(0) != '#'*/) {
                href = menuItem.url;
                if (href.indexOf($('base').attr('href')) != 0 && href.indexOf('http') != 0) {
                    href = $('base').attr('href') + href;
                }
                href = href.replace("//", "/").replace("#", "");
//            else if (menuItem.object == 'custom' && menuItem.url.charAt(0) == '#')
//                href = menuItem.url.substring(1, menuItem.url.length);
            } else
                href = menuItem.url;

            if (href.charAt(href.length - 1) == '/')
                href = href.substr(0, href.length - 1);
            if (href.length == 0)
                href = $('base').attr('href');

            return href;
        };
        $scope.getClass = function (menuItem) {
            var path = $scope.getUrl(menuItem);
            path = path.replace($('base').attr('href'), "");
            var pathParts = path.split('?');
            if (($location.path() === "/" + pathParts[0] || $location.path() === pathParts[0]) && path != "") {
                var urlAnchor = "", linkAnchor = "";
                //Getting anchor from link href
                if (pathParts.length > 1) {
                    pathParts = pathParts[1].split('sec=');
                    if (pathParts.length > 1) {
                        pathParts = pathParts [1].split('&');
                        linkAnchor = pathParts[0];
                    }
                }
                //Getting anchor from url
                if ($location.search().sec) {
                    urlAnchor = $location.search().sec;
                }

                return urlAnchor == linkAnchor ? 'active' : '';
            } else {
                if (menuItem.children.length > 0) {
                    for (var i = 0; i < menuItem.children.length; i++) {
                        var tmp = $scope.getClass(menuItem.children[i]);
                        if (tmp != "")
                            return tmp;
                    }
                }
            }
            return '';
        };
        $scope.getAnchor = function (url) {
            var parts = url.split('?sec=');
            if (parts.length > 1) {
                parts = parts[1].split('&');
                return parts[0];
            } else {
                return "";
            }
        };
    }]);

    //Main controller
    app.controller('Home', ['$scope', '$http', '$route', '$location', '$anchorScroll', '$timeout', '$document', '$rootScope', function ($scope, $http, $route, $location, $anchorScroll, $timeout, $document, $rootScope) {

        $scope.$watch(function () {
            return $location.search()
        }, function () {
            $scope.onSectionChange();
        }, true);

        var times = 15;
        $scope.onSectionChange = function () {
//            console.log('sectionChange' + ' ' + $rootScope.fluid);
            if (!$rootScope.fluid) {
//                console.log('scrolling')
                $rootScope.changingAnchor = true;
                var height = 75;
                if ($(window).width() >= 768 && $('body').hasClass('customize-support')) {
                    height = 95;
                } else if ($(window).width() < 768 && $('body').hasClass('customize-support')) {
                    height = 108;
                }

                var searchObject = $location.search();

                var anchor = searchObject['sec'];
                if (!anchor) {
                    $document.scrollTop(-height, 300);
                    return;
                }
                var elem = $(document).find('#' + anchor);
                if (elem.length > 0 && elem.attr('ready') != undefined && elem.attr('ready') != true) {
                    $document.scrollToElement(elem, height, 300).then(function(){
                        $timeout(function () {
                            $rootScope.changingAnchor = false;
                        }, 200);
//                        console.log('stop scrolling')
                    });
                } else {
                    if (times > 0)
                        $timeout(function () {
                            $scope.onSectionChange();
                        }, 500);
                    times--;
                }
            } else {
                $rootScope.fluid = false;
            }
        };
        $timeout(function () {
            console.log('timeout')
            $scope.onSectionChange();
        });
    }]);

    //Content controller
    app.controller('PostCtrl', ['$scope', '$route', '$routeParams', '$http', '$sce', function ($scope, $route, $routeParams, $http, $sce) {
        $http.get(urlBase + 'posts?filter[name]=' + $routeParams.postName)
            .success(function (res) {
                if (res.length == 0) {
                    $scope.error404 = true;
                } else {
                    $scope.post = res[0];
                    console.log($scope.post);
                    $sce.trustAsHtml($scope.post.content.rendered);
                    document.querySelector('title').innerHTML = $('#baseTitle').val() + " - " + $sce.trustAsHtml(res[0].title.rendered);
                }
            }).error(function (error) {
                $scope.error404 = true;
            });
        $scope.asd = "<span>asd</span>"
    }]);

    //Content controller
    app.controller('PageCtrl', ['$scope', '$route', '$routeParams', '$http', '$sce', function ($scope, $route, $routeParams, $http, $sce) {
        $http.get(urlBase + 'pages?filter[name]=' + $routeParams.postName)
            .success(function (res) {
                if (res.length == 0) {
                    $scope.error404 = true;
                } else {
                    $scope.post = res[0];
                    $sce.trustAsHtml($scope.post.content.rendered);
                    document.querySelector('title').innerHTML = $('#baseTitle').val() + " - " + $sce.trustAsHtml(res[0].title.rendered);
                }
            }).error(function (error) {
                $scope.error404 = true;
            });
        $scope.asd = "<span>asd</span>"
    }]);

})(window.angular);