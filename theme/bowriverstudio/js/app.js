(function (angular) {
    'use strict';
    window.urlBase = "wp-json/wp/v2/";
    window.app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngAnimate', 'duScroll']);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: myLocalized.partials + 'main.html',
                controller: 'Home',
                title: 'Home',
                level: 0,
                reloadOnSearch: false
            })
            .when('/:postName', {
                templateUrl: myLocalized.partials + 'content.html',
                controller: 'PageCtrl',
                title: '',
                resolve: {
                    load: ['$q', '$routeParams', '$rootScope', '$route', function ($q, $routeParams, $rootScope, $route) {
                        var defer = $q.defer();
                        defer.resolve();

                        $rootScope.lastLevel = $route.current.$$route.level;
                        return defer.promise;
                    }]
                },
                reloadOnSearch: false
            })
            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    }]);

    app.run(['$rootScope', '$http', '$sce', '$titleService', function ($rootScope, $http, $sce, $titleService) {
        $rootScope.myLocalized = window.myLocalized;
        $rootScope.$titleService = $titleService;
        $rootScope.renderHtml = function (html) {
            return $sce.trustAsHtml(html);
        };
    }]);

    app.run(function ($rootScope, $location) {
        if (!window.history || !history.replaceState) {
            return;
        }
        $rootScope.$on('duScrollspy:becameActive', function ($event, $element, $target) {
//            console.log('becameActive')
            if (!$rootScope.changingAnchor) {
                $rootScope.fluid = true;
                $rootScope.$apply(function () {
                    var searchObject = {};
                    var parts = $element.attr('href').split('?');
                    if (parts.length > 1) {
                        parts = parts[1].split('&');
                        for (var i = 0; i < parts.length; i++) {
                            var p1 = parts[i].split('=');
                            searchObject[p1[0]] = p1[1];
                        }
                    }
                    $location.search(searchObject);
                });
            }
        });
    });

    app.factory('$titleService', function () {
        return {
            getTitle: function (original, gray) {
                if (!original)
                    return;
                var parts = original.split(' ');
                if (gray) {
                    if (parts.length > 1)
                        return parts[0];
                    else return ""
                } else {
                    if (parts.length > 1)
                        return original.substring(parts[0].length + 1);
                    else return original
                }
            }
        };
    });

    app.filter('highlight', function ($sce) {
        return function (text, phrase) {
            if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
                '<span class="highlighted">$1</span>')

            return $sce.trustAsHtml(text)
        }
    })

})(window.angular);