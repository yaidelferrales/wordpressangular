<!doctype html>
<html lang="en" ng-app="app">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bow River Web Design</title>

        <meta name="description" content="Web Development">
        <meta name="keywords" content="web, development, bow river">

        <?php $base = "/"; ?>
        <base href="<?php echo $base; ?>">

        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/imgs/favicon.ico">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <input type="hidden" id="baseTitle" value="<?php echo (get_bloginfo ('name', 'raw')); ?>"/>

        <div id="page">
            <!--MAIN NAVBAR-->
            <nav class="navbar navbar-default navbar-fixed-top" ng-controller="MenuCtrl" ng-init="init('primary')">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="<?php echo $base; ?>" du-scrollspy="top-page">
                            <img alt="Brand" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/navbar-logo.png">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav"></ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown styled-dropdown" ng-class="getClass(menuItem)" ng-repeat="menuItem in menuItems">
                                <a ng-href="{{getUrl(menuItem)}}" role="button" aria-haspopup="true" aria-expanded="false" du-scrollspy="{{getAnchor(getUrl(menuItem))}}" offset="108"> <!--class="dropdown-toggle"-->
                                    <span ng-bind-html="menuItem.title"></span>
                                    <i class="dropdown-caret fa fa-angle-down fa-2x" ng-if="menuItem.children.length > 0"></i>
                                </a>
                                <ul class="dropdown-menu" ng-if="menuItem.children.length > 0">
                                    <li ng-repeat="subMenuItem in menuItem.children" ng-class="getClass(subMenuItem)"><a ng-href="{{getUrl(subMenuItem)}}"><i class="fa fa-arrow-right text-gray-lighter hidden-md hidden-lg"></i>{{subMenuItem.title}}</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <div class="view-container" style="padding-top: 75px;" id="top-page">
                <div ng-view class="<!--mfade-->"></div>
            </div>

            <footer>
                <div class="container-fluid">
                    <div class="container">
                        <div class="row margin-top-double margin-bottom-double">

                            <div class="col-sm-3 col-xs-12">
                                <p class="text-bowriver-color3">
                                    Wanna meet us?
                                </p>
                                <p class="">
                                    421 13 ST NW<br/>
                                    Calgary AB T2N 1Z3<br/>
                                    Canada <br/>
                                    403 519-0274
                                </p>
                            </div>

                            <div class="col-sm-3 col-xs-12" ng-controller="MenuCtrl" ng-init="init('more')">
                                <p class="text-bowriver-color3 hidden-xs">
                                    More About Us
                                </p>
                                <ul style="list-style: none; padding: 0;">
                                    <li ng-repeat="menuItem in menuItems" >
                                        <a ng-href="{{getUrl(menuItem)}}" ng-bind-html="menuItem.title"></a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-sm-6 col-xs-12 text-right">
                                <img class="margin-top-simple fit-to-container" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/footer-logo.png" alt="LOGO"/>
                            </div>

                            <div class="col-xs-12 text-center">
                                <h6 class="text-bowriver-color4 no-margin-bottom">
                                    © 2016 BowRiver Web Design. All rights reserved
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!--DIMMER-->

        <!--dimmer that is shown when the database is been fetched to check product availability-->
        <div class="dimmer-loader hidden"><i class="fa fa-refresh fa-3x text-white center-block"></i></div>

        <!--MODALS-->

        <!--confirmation modal-->
        <div class="modal fade" id="confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="confirmation-modal" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Confirmation Modal</h4>
                    </div>
                    <div class="modal-body">

                        Modal Text

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                        <button type="button" class="btn btn-default">YES</button>
                    </div>
                </div>
            </div>
        </div>

        <?php wp_head(); ?>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
        <?php wp_footer(); ?>        
    </body>
</html>